#!/usr/bin/env bash
set -e

groupadd groupe_a
groupadd groupe_b
groupadd groupe_c
groupadd admin

# -s /bin/zsh
useradd -m -g admin -G groupe_a,groupe_b,groupe_c admin
useradd -m -g groupe_a -G groupe_c lambda_a
useradd -m -g groupe_b -G groupe_c lambda_b

passwd admin <<<$'a\na\n'
passwd lambda_a <<<$'a\na\n'
passwd lambda_b <<<$'b\nb\n'

mkdir -p share
cd share
mkdir -p dir_{a,b,c}

chown admin:admin .
chown admin:groupe_a dir_a
chown admin:groupe_b dir_b
chown admin:groupe_c dir_c

chmod 0755 .
chmod 3770 dir_a
chmod 3770 dir_b
chmod 2750 dir_c

su admin -c "$(
  cat <<EOF
  cd
  touch passwd
  # echo "$(id -u admin):adm" >>passwd
  # echo "$(id -u lambda_a):aa" >>passwd
  # echo "$(id -u lambda_b):bb" >>passwd
EOF
)" <<<a 2>/dev/null
