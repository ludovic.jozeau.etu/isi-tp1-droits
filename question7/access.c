#include  <unistd.h>
#include <assert.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
    assert(argc==2);
    assert(access(argv[1], F_OK)==0);
    
    int status;
    char acces[3];
    for(int i=0; i<3; i++)
    {
        switch(i)
        {
            case 1:
                status=access(argv[1],W_OK);
                if (status==0)
                    acces[i]='w';
                break;
            case 2:
                status=access(argv[1],X_OK);
                if (status==0)
                    acces[i]='x';
                break;
            default:
                status=access(argv[1],R_OK);
                if (status==0)
                    acces[i]='r';
                break;
        };
        if (status==(-1))
            acces[i]='-';
    }
    printf("%s\n",acces);
    return 0;
}
