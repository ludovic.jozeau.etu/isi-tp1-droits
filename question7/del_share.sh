#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

rm -r share

userdel -r admin
userdel -r lambda_a
userdel -r lambda_b

groupdel groupe_a
groupdel groupe_b
groupdel groupe_c
groupdel admin || echo "admin group already removed"
