#!/usr/bin/env bash

function setup_env() {
  su admin -c 'echo "owner $USER in dir_a" > share/dir_a/test_admin' <<<a 2>/dev/null
  su admin -c 'echo "owner $USER in dir_b" > share/dir_b/test_admin' <<<a 2>/dev/null
  su admin -c 'echo "owner $USER in dir_c" > share/dir_c/test_admin' <<<a 2>/dev/null

  su lambda_a -c 'echo "owner $USER in dir_a" > share/dir_a/test_lambda_a' <<<a 2>/dev/null
  su lambda_a -c 'echo "owner $USER in dir_b" > share/dir_b/test_lambda_a' <<<a 2>/dev/null
  su lambda_a -c 'echo "owner $USER in dir_c" > share/dir_c/test_lambda_a' <<<a 2>/dev/null

  su lambda_b -c 'echo "owner $USER in dir_a" > share/dir_a/test_lambda_b' <<<b 2>/dev/null
  su lambda_b -c 'echo "owner $USER in dir_b" > share/dir_b/test_lambda_b' <<<b 2>/dev/null
  su lambda_b -c 'echo "owner $USER in dir_c" > share/dir_c/test_lambda_b' <<<b 2>/dev/null
}

function clean_env() {
  su admin -c "rm share/dir_a/test_admin" <<<a 2>/dev/null
  su admin -c "rm share/dir_b/test_admin" <<<a 2>/dev/null
  su admin -c "rm share/dir_c/test_admin" <<<a 2>/dev/null

  su admin -c "rm share/dir_a/test_lambda_a" <<<a 2>/dev/null
  su admin -c "rm share/dir_b/test_lambda_a" <<<a 2>/dev/null
  su admin -c "rm share/dir_c/test_lambda_a" <<<a 2>/dev/null

  su admin -c "rm share/dir_a/test_lambda_b" <<<a 2>/dev/null
  su admin -c "rm share/dir_b/test_lambda_b" <<<a 2>/dev/null
  su admin -c "rm share/dir_c/test_lambda_b" <<<a 2>/dev/null
  return 0
}

# TODO: return non 0 if 1 test have failed
# TODO: summary of the tests

setup_env
su admin -c "$(
  cat <<EOF
  source "$(dirname "$0")/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug share
  echo "^^^=env=^^^"

  can 'touch share/dir_a/test_a'
  can 'touch share/dir_b/test_b'
  can 'touch share/dir_c/test_c'

  can 'cat share/dir_a/test_admin'
  can 'cat share/dir_a/test_lambda_a'
  can 'cat share/dir_b/test_admin'
  can 'cat share/dir_b/test_lambda_b'
  can 'cat share/dir_c/test_admin'

  can 'echo salut >>share/dir_a/test_admin'
  can_or_not 'echo salut >>share/dir_a/test_lambda_a'
  can 'echo salut >>share/dir_b/test_admin'
  can_or_not 'echo salut >>share/dir_b/test_lambda_b'
  can 'echo salut >>share/dir_c/test_admin'

  can 'rm share/dir_a/test_a'
  can 'rm share/dir_a/test_admin'
  can 'rm share/dir_a/test_lambda_a'
  can 'rm share/dir_b/test_b'
  can 'rm share/dir_b/test_admin'
  can 'rm share/dir_b/test_lambda_b'
  can 'rm share/dir_c/test_c'
  can 'rm share/dir_c/test_admin'

EOF
)" <<<a 2>/dev/null
clean_env

echo

setup_env
su lambda_a -c "$(
  cat <<EOF
  source "$(dirname "$0")/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug share
  echo "^^^=env=^^^"

  can 'touch share/dir_a/test_a'
  cannot 'touch share/dir_b/test_b'
  cannot 'touch share/dir_c/test_c'

  can 'cat share/dir_a/test_admin'
  can 'cat share/dir_a/test_lambda_a'
  cannot 'cat share/dir_b/test_admin'
  cannot 'cat share/dir_b/test_lambda_b'
  can 'cat share/dir_c/test_admin'

  can_or_not 'echo salut >>share/dir_a/test_admin'
  can 'echo salut >>share/dir_a/test_lambda_a'
  cannot 'echo salut >>share/dir_b/test_admin'
  cannot 'echo salut >>share/dir_b/test_lambda_b'
  cannot 'echo salut >>share/dir_c/test_admin'

  can 'rm share/dir_a/test_a'
  cannot 'rm share/dir_a/test_admin'
  can 'rm share/dir_a/test_lambda_a'
  cannot 'rm share/dir_b/test_b'
  cannot 'rm share/dir_b/test_admin'
  cannot 'rm share/dir_b/test_lambda_b'
  cannot 'rm share/dir_c/test_c'
  cannot 'rm share/dir_c/test_admin'

EOF
)" <<<a 2>/dev/null
clean_env

echo

setup_env
su lambda_b -c "$(
  cat <<EOF
  source "$(dirname "$0")/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug share
  echo "^^^=env=^^^"

  cannot 'touch share/dir_a/test_a'
  can 'touch share/dir_b/test_b'
  cannot 'touch share/dir_c/test_c'

  cannot 'cat share/dir_a/test_admin'
  cannot 'cat share/dir_a/test_lambda_a'
  can 'cat share/dir_b/test_admin'
  can 'cat share/dir_b/test_lambda_b'
  can 'cat share/dir_c/test_admin'

  cannot 'echo salut >>share/dir_a/test_admin'
  cannot 'echo salut >>share/dir_a/test_lambda_a'
  can_or_not 'echo salut >>share/dir_b/test_admin'
  can 'echo salut >>share/dir_b/test_lambda_b'
  cannot 'echo salut >>share/dir_c/test_admin'

  cannot 'rm share/dir_a/test_a'
  cannot 'rm share/dir_a/test_admin'
  cannot 'rm share/dir_a/test_lambda_a'
  can 'rm share/dir_b/test_b'
  cannot 'rm share/dir_b/test_admin'
  can 'rm share/dir_b/test_lambda_b'
  cannot 'rm share/dir_c/test_c'
  cannot 'rm share/dir_c/test_admin'

EOF
)" <<<b 2>/dev/null
clean_env
