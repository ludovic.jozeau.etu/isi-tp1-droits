#!/usr/bin/env bash

function err() {
  printf "\e[31;1m%s\e[0m\n" "$*"
}

function ok() {
  printf "\e[32;1m%s\e[0m\n" "$*"
}

function log() {
  printf "\e[33;1m%s\e[0m\n" "$*"
}

# evaluate arguments and check return value for 0
function can() {
  local output
  local ret
  output=$(eval "$*" 2>&1)
  ret=$?
  if ((ret != 0)); then
    echo
    err "vvvv=Unexepected $USER can't do : $*=vvvv"
    echo "$output"
    err "^^^^=Unexepected $USER can't do : $*=^^^^"
    echo
    return $ret
  else
    ok "=====As expected $USER can do   : $*====="
    return 0
  fi
}

# evaluate arguments and check return value for non 0
function cannot() {
  local output
  local ret
  output=$(eval "$*" 2>&1)
  ret=$?
  if ((ret == 0)); then
    echo
    err "vvvv=Unexepected $USER can do   : $*=vvvv"
    echo "$output"
    err "^^^^=Unexepected $USER can do   : $*=^^^^"
    echo
    return $ret
  else
    ok "=====As expected $USER can't do : $*====="
    return 0
  fi
}

# evaluate arguments and check return value, log if non 0 is returned
function can_or_not() {
  local output
  local ret
  output=$(eval "$*" 2>&1)
  ret=$?
  if ((ret != 0)); then
    echo
    log "vvvv=This time $USER can't do   : $*=vvvv"
    echo "$output"
    log "^^^^=This time $USER can't do   : $*=^^^^"
    echo
    return $ret
  else
    ok "=====As expected $USER can do   : $*====="
    return 0
  fi
}

function expect() {
  local res
  output=$(eval "$1" 2>&1)
  if diff <(echo "$output") <(echo "$2"); then
    ok "as expected: got output: $output"
  else
    err "expected: '$2'"
    log "got output: $output"
  fi
}

function match() {
  local res
  output=$(eval "$1" 2>&1)
  if [[ "$(echo "$output")" == *"$(echo "$2")"* ]]; then
    ok "as expected: output: $output"
  else
    err "expected to contains: '$2'"
    log "but got output: $output"
  fi
}

function exists() {
  if [[ -e "$1" ]]; then
    ok "as expected: $1 exists"
  else
    err "unexpected: $1 doesn't exists!"
  fi
}

function removed() {
  if ! [[ -e "$1" ]]; then
    ok "as expected: $1 doesn't exists"
  else
    err "unexpected: $1 exists!"
  fi
}

function setup_env() {
  su admin -c 'echo "owner $USER in dir_a" > ../question7/share/dir_a/test_admin' <<<a 2>/dev/null
  su admin -c 'echo "owner $USER in dir_b" > ../question7/share/dir_b/test_admin' <<<a 2>/dev/null
  su admin -c 'echo "owner $USER in dir_c" > ../question7/share/dir_c/test_admin' <<<a 2>/dev/null

  su lambda_a -c 'echo "owner $USER in dir_a" > ../question7/share/dir_a/test_lambda_a' <<<a 2>/dev/null
  su lambda_a -c 'echo "owner $USER in dir_b" > ../question7/share/dir_b/test_lambda_a' <<<a 2>/dev/null
  su lambda_a -c 'echo "owner $USER in dir_c" > ../question7/share/dir_c/test_lambda_a' <<<a 2>/dev/null

  su lambda_b -c 'echo "owner $USER in dir_a" > ../question7/share/dir_a/test_lambda_b' <<<b 2>/dev/null
  su lambda_b -c 'echo "owner $USER in dir_b" > ../question7/share/dir_b/test_lambda_b' <<<b 2>/dev/null
  su lambda_b -c 'echo "owner $USER in dir_c" > ../question7/share/dir_c/test_lambda_b' <<<b 2>/dev/null
}

function clean_env() {
  su admin -c "rm ../question7/share/dir_a/test_admin" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_b/test_admin" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_c/test_admin" <<<a 2>/dev/null

  su admin -c "rm ../question7/share/dir_a/test_lambda_a" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_b/test_lambda_a" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_c/test_lambda_a" <<<a 2>/dev/null

  su admin -c "rm ../question7/share/dir_a/test_lambda_b" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_b/test_lambda_b" <<<a 2>/dev/null
  su admin -c "rm ../question7/share/dir_c/test_lambda_b" <<<a 2>/dev/null
  return 0
}
