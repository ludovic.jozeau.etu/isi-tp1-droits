cmake_minimum_required(VERSION 3.15)
project(
  isi-tp1-droits
  VERSION 0.1.0
  DESCRIPTION ""
  HOMEPAGE_URL ""
  LANGUAGES C CXX
)

find_package(Boost REQUIRED COMPONENTS system thread)

add_executable(suid)
target_sources(suid PRIVATE question3/suid.c)
target_compile_features(suid PRIVATE c_std_11)

add_library(passwd_db)
target_sources(
  passwd_db PRIVATE question8/check_pass.cpp question8/Password.cpp
)
target_include_directories(passwd_db PUBLIC question8)
target_compile_features(passwd_db PUBLIC cxx_std_17)
target_link_libraries(passwd_db PRIVATE crypt)

add_library(group)
target_sources(group PRIVATE question8/group.cpp)
target_include_directories(group PUBLIC question8)
target_compile_features(group PUBLIC cxx_std_17)

add_executable(rmg)
target_sources(rmg PRIVATE question8/rmg.cpp)
target_link_libraries(rmg PRIVATE passwd_db group)
target_compile_features(rmg PRIVATE cxx_std_17)
set(SHARE_FOLDER
    "${CMAKE_CURRENT_LIST_DIR}/question7/share"
    CACHE PATH "share folder where dir_a dir_b and dir_c are"
)
message("==> Using ${SHARE_FOLDER} as share folder")
target_compile_definitions(rmg PRIVATE SHARE_FOLDER="${SHARE_FOLDER}")

add_executable(pwg)
target_sources(pwg PRIVATE question9/pwg.cpp)
target_compile_features(pwg PRIVATE cxx_std_17)
target_link_libraries(pwg PRIVATE passwd_db group)

add_executable(group_client)
target_sources(group_client PRIVATE question10/client.cpp)
target_compile_features(group_client PRIVATE cxx_std_17)
target_link_libraries(
  group_client PRIVATE Boost::boost Boost::system Boost::thread
)
set(SERVER_ADDR
    localhost
    # 172.28.100.218
    CACHE STRING "server address"
)
message("==> Using ${SERVER_ADDR} as server address")
target_compile_definitions(group_client PRIVATE SERVER_ADDR="${SERVER_ADDR}")

add_executable(group_server)
target_sources(
  group_server PRIVATE question10/group_server.cpp question10/user.cpp
)
target_compile_features(group_server PRIVATE cxx_std_17)
target_link_libraries(
  group_server
  PRIVATE passwd_db
          Boost::boost
          Boost::system
          Boost::thread
)
target_compile_definitions(group_server PRIVATE SHARE_FOLDER="${SHARE_FOLDER}")
