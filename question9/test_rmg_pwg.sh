#!/usr/bin/env bash

cd "$(dirname "$0")"
source "$(dirname "$0")/../question7/test_share_utils.sh"

echo "================"
echo "========resetting passwd file========"
echo "================"

su admin -c 'printf '' > /home/admin/passwd' <<<a 2>/dev/null

rmg="../question8/rmg"
pwg="./pwg"
share="../question7/share"

setup_env
su admin -c "$(
  cat <<EOF
  source "$(dirname "$0")/../question7/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug $share
  echo "^^^=env=^^^"

  cannot '$rmg $share/dir_a/test_admin'
  cannot '$rmg $share/dir_a/test_lambda_a'
  cannot '$rmg $share/dir_b/test_admin'
  cannot '$rmg $share/dir_b/test_lambda_b'
  cannot '$rmg $share/dir_c/test_admin'
  cannot '$rmg $share/'

  exists '$share/dir_a/test_admin'
  exists '$share/dir_a/test_lambda_a'
  exists '$share/dir_b/test_admin'
  exists '$share/dir_b/test_lambda_b'
  exists '$share/dir_c/test_admin'

  expect '$rmg $share/dir_a/test_admin' "You aren't in the passord database!"
  match '$rmg $share/dir_c/test_admin' "can only delete files in"

  exists '$share/dir_a/test_admin'
  exists '$share/dir_c/test_admin'

  can "$pwg<<<\$'admin\nadmin\n'"
  can "$pwg<<<\$'admin\nad\nad\n'"
  cat /home/admin/passwd

  can "$rmg $share/dir_a/test_admin<<<\$'ad\n'"
  can "$rmg $share/dir_a/test_lambda_a<<<\$'ad\n'"
  can "$rmg $share/dir_b/test_admin<<<\$'ad\n'"
  can "$rmg $share/dir_b/test_lambda_b<<<\$'ad\n'"
  cannot "$rmg $share/dir_c/test_admin<<<\$'ad\n'"
  cannot "$rmg $share/<<<\$'ad\n'"

  removed '$share/dir_a/test_admin'
  removed '$share/dir_a/test_lambda_a'
  removed '$share/dir_b/test_admin'
  removed '$share/dir_b/test_lambda_b'
  exists '$share/dir_c/test_admin'

EOF
)" <<<a
clean_env

echo

setup_env
su lambda_a -c "$(
  cat <<EOF
  source "$(dirname "$0")/../question7/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug $share
  echo "^^^=env=^^^"

  cannot '$rmg $share/dir_a/test_admin'
  cannot '$rmg $share/dir_a/test_lambda_a'
  cannot '$rmg $share/dir_b/test_admin'
  cannot '$rmg $share/dir_b/test_lambda_b'
  cannot '$rmg $share/dir_c/test_admin'
  cannot '$rmg $share/'

  exists '$share/dir_a/test_admin'
  exists '$share/dir_a/test_lambda_a'
  exists '$share/dir_c/test_admin'

  expect '$rmg $share/dir_a/test_admin' "You aren't in the passord database!"
  match '$rmg $share/dir_b/test_admin' "You aren't part of the group of"
  match '$rmg $share/dir_c/test_admin' "can only delete files in"

  exists '$share/dir_a/test_admin'
  exists '$share/dir_c/test_admin'

  can "$pwg<<<\$'la\nla\n'"
  match '$rmg $share/dir_a/test_admin<<<\$'wrong\n'' "Wrong password"
  exists '$share/dir_a/test_admin'
  can "$pwg<<<\$'la\nlambdaa\nlambdaa\n'"

  can "$rmg $share/dir_a/test_admin<<<\$'lambdaa\n'"
  can "$rmg $share/dir_a/test_lambda_a<<<\$'lambdaa\n'"
  cannot "$rmg $share/dir_b/test_admin<<<\$'lambdaa\n'"
  cannot "$rmg $share/dir_b/test_lambda_b<<<\$'lambdaa\n'"
  cannot "$rmg $share/dir_c/test_admin<<<\$'lambdaa\n'"
  cannot "$rmg $share/<<<\$'lambdaa\n'"

  removed '$share/dir_a/test_admin'
  removed '$share/dir_a/test_lambda_a'
  exists '$share/dir_c/test_admin'

EOF
)" <<<a 2>/dev/null
clean_env

echo

setup_env
su lambda_b -c "$(
  cat <<EOF
  source "$(dirname "$0")/../question7/test_share_utils.sh"

  echo "================"
  echo "========Testing \$USER========"
  echo "================"

  echo "vvv=env=vvv"
  tree -pug $share
  echo "^^^=env=^^^"

  cannot '$rmg $share/dir_a/test_admin'
  cannot '$rmg $share/dir_a/test_lambda_a'
  cannot '$rmg $share/dir_b/test_admin'
  cannot '$rmg $share/dir_b/test_lambda_b'
  cannot '$rmg $share/dir_c/test_admin'
  cannot '$rmg $share/'

  exists '$share/dir_b/test_admin'
  exists '$share/dir_b/test_lambda_b'
  exists '$share/dir_c/test_admin'

  expect '$rmg $share/dir_b/test_admin' "You aren't in the passord database!"
  match '$rmg $share/dir_a/test_admin' "You aren't part of the group of"
  match '$rmg $share/dir_c/test_admin' "can only delete files in"

  exists '$share/dir_b/test_admin'
  exists '$share/dir_c/test_admin'

  can "$pwg<<<\$'lb\nlb\n'"
  match '$rmg $share/dir_b/test_admin<<<\$'wrong\n'' "Wrong password"
  exists '$share/dir_b/test_admin'
  can "$pwg<<<\$'lb\nlambdab\nlambdab\n'"

  cannot "$rmg $share/dir_a/test_admin<<<\$'lambdab\n'"
  cannot "$rmg $share/dir_a/test_lambda_a<<<\$'lambdab\n'"
  can "$rmg $share/dir_b/test_admin<<<\$'lambdab\n'"
  can "$rmg $share/dir_b/test_lambda_b<<<\$'lambdab\n'"
  cannot "$rmg $share/dir_c/test_admin<<<\$'lambdab\n'"
  cannot "$rmg $share/<<<\$'lambdab\n'"

  removed '$share/dir_b/test_admin'
  removed '$share/dir_b/test_lambda_b'
  exists '$share/dir_c/test_admin'

EOF
)" <<<b 2>/dev/null
clean_env

su admin -c "cat /home/admin/passwd" <<<a
