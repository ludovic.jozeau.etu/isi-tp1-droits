#include "check_pass.hpp"
#include "group.hpp"

#include <cerrno>
#include <iostream>

// References:
// https://github.com/shadow-maint/shadow/blob/e8a2cfa7dc889953c1160bf6e2593e9e7e53b0a4/libmisc/salt.c#L403
// Why use yescrypt: https://github.com/hashcat/hashcat/issues/2816
int main(int argc, char** argv)
{
  bool gid_check{false};
  for (gid_t gid : isi::getgroups())
  {
    std::string fg_name = isi::getgroup_name(gid);
    if (fg_name == "groupe_a" || fg_name == "groupe_b")
    {
      gid_check = true;
    }
  }
  if (!gid_check)
  {
    std::cerr << "You are not in groupe_a nor in groupe_b\n";
    std::exit(EACCES);
  }

  isi::Passwd_db pdb;
  if (pdb.change_pw())
  {
    std::cout << "Password changed\n";
  }
  else
  {
    std::cout << "Password unchanged\n";
    std::exit(EXIT_FAILURE);
  }
}