#!/usr/bin/env bash

cd "$(dirname "$0")"

rm -f pwg
cp ../build/pwg .
sudo chown admin:admin pwg
sudo chmod u+s pwg
