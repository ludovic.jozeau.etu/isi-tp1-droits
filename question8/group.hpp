#pragma once

#include <filesystem>
#include <string>
#include <vector>

namespace isi
{

namespace fs = std::filesystem;

std::string getgroup_name(gid_t gid);

std::vector<gid_t> getgroups();

gid_t getgroup(fs::path const& fpath);

bool is_in_user_group(gid_t gid);
} // namespace isi
