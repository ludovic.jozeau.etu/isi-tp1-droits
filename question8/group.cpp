#include "group.hpp"

#include <grp.h>
#include <pwd.h>
#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>

namespace isi
{

std::string getgroup_name(gid_t gid)
{
  errno = 0;
  group* grp = getgrgid(gid);
  if (!grp)
  {
    perror("getgroup_name getgrgid");
    return std::to_string(gid);
  }
  return grp->gr_name;
}

std::vector<gid_t> getgroups()
{
  errno = 0;
  uid_t uid = getuid();

  passwd* pw = getpwuid(uid);
  if (!pw)
  {
    perror("getgroups getpwuid");
    return {};
  }

  int ngroups = 0;

  // this call is just to get the correct ngroups
  getgrouplist(pw->pw_name, pw->pw_gid, nullptr, &ngroups);
  std::vector<gid_t> groups(ngroups);

  // here we actually get the groups
  getgrouplist(pw->pw_name, pw->pw_gid, groups.data(), &ngroups);

  return groups;
}

gid_t getgroup(fs::path const& fpath)
{

  struct stat st;
  if (stat(fpath.c_str(), &st) == -1)
  {
    std::perror("getgroup stat");
    std::exit(errno);
  }

  return st.st_gid;
}

bool is_in_user_group(gid_t gid)
{
  auto grps = getgroups();
  return std::count(std::begin(grps), std::end(grps), gid) != 0;
}

} // namespace isi
