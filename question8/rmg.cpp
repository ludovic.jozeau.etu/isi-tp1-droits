#include "check_pass.hpp"
#include "group.hpp"

#include <algorithm>
#include <cerrno>
#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

namespace fs = std::filesystem;

int main(int argc, char** argv)
{
  if (argc != 2)
  {
    std::cout << "usage: " << argv[0] << " file\n";
    std::exit(EXIT_FAILURE);
  }

  fs::path fpath{argv[1]};
  if (!fs::exists(fpath))
  {
    std::cerr << fpath << " doesn't exist\n";
    std::exit(ENOENT);
  }

  fs::path share_folder{SHARE_FOLDER};
  if (!fs::exists(share_folder))
  {
    std::cerr << share_folder << " doesn't exist\n";
    std::exit(ENOENT);
  }

  fpath = fs::canonical(fpath);
  share_folder = fs::canonical(share_folder);
  fs::path rel_to_share = fs::relative(fpath, share_folder);
  if (!((*rel_to_share.begin() == "dir_a" || *rel_to_share.begin() == "dir_b") &&
        rel_to_share.filename() != "dir_a" && rel_to_share.filename() != "dir_b"))
  {
    std::cerr << argv[0] << " can only delete files in " << share_folder.c_str()
              << "/{dir_a, dir_b}\n";
    std::exit(EACCES);
  }

  gid_t       fgid = isi::getgroup(fpath);
  std::string fg_name = isi::getgroup_name(fgid);

  if (fg_name != "groupe_a" && fg_name != "groupe_b")
  {
    std::cerr << argv[0] << " can only delete files from groupe_a or groupe_b\n";
    std::exit(EACCES);
  }

  if (!isi::is_in_user_group(fgid))
  {
    std::cerr << "You aren't part of the group of " << fpath << " which is " << fg_name
              << "\n";
    std::exit(EACCES);
  }

  isi::Passwd_db pw_db;
  if (!pw_db.find_user())
  {
    std::cerr << "You aren't in the passord database!\n";
    std::exit(EACCES);
  }

  if (pw_db.check_pw())
  {
    fs::remove_all(fpath);
    std::cout << "deleted " << fpath << "\n";
  }
  else
  {
    std::cerr << "Wrong password\n";
    std::exit(EXIT_FAILURE);
  }
}
