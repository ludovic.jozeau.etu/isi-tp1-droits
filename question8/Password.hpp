#pragma once

#include <termios.h>
#include <unistd.h>

#include <string>
#include <string_view>

namespace isi
{

/**
 * @brief no echo tty RAII lock
 *
 */
class Password_mode
{
private:
  termios tty;

public:
  Password_mode()
  {
    tcgetattr(STDIN_FILENO, &tty);
    termios tty_copy = tty;
    tty_copy.c_lflag &= ~ECHO;
    tcsetattr(STDIN_FILENO, TCSANOW, &tty_copy);
  }

  ~Password_mode()
  {
    tcsetattr(STDIN_FILENO, TCSANOW, &tty);
  }
};

/**
 * @brief zero erase on deallocate
 *
 */
class password_allocator : public std::allocator<char>
{
public:
  using std::allocator<char>::allocator;

  template <typename Tp1>
  struct rebind
  {
    using other = password_allocator;
  };
  // password_allocator(std::allocator<char> const& a) : std::allocator<char>(a) {}

  void deallocate(char* str, size_type n)
  {
    std::fill_n(str, n, 0);
    std::allocator<char>::deallocate(str, n);
  }
};

using Password_base = std::basic_string<char, std::char_traits<char>, password_allocator>;

/**
 * @brief string wrapper that will zero erase it's charaters
 *
 */
class Password : public Password_base
{
public:
  Password() noexcept : Password_base(password_allocator{}) {}
  Password(char const* cstr) : Password_base(cstr, password_allocator{}) {}
  Password(std::string&& str) : Password_base(std::move(str), password_allocator{}) {}

  Password(Password const& other) = default;
  Password(Password&& other) = default;
  Password& operator=(Password const& other) = default;
  Password& operator=(Password&& other) = default;

  ~Password()
  {
    std::fill(begin(), end(), 0);
  }
};

bool is_pw_equal(std::string_view lhs, std::string_view rhs);

Password hash_pw(Password&& pw);

Password hash_pw(Password&& pw, Password const& salt);

Password ask_pw(std::string_view question);

} // namespace isi