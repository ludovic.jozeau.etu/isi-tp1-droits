#include "check_pass.hpp"
#include "Password.hpp"

#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

namespace isi
{
namespace fs = std::filesystem;
namespace
{
fs::path const pw_path{"/home/admin/passwd"};
}

Passwd_db::Passwd_db(uid_t uid) : uid(uid), pwf(pw_path)
{
  if (!pwf)
  {
    std::cerr << "error opening " << pw_path << " file\n";
    std::exit(EIO);
  }
}

bool Passwd_db::find_user()
{
  pwf.clear();
  pwf.seekg(user_pos);
  if (!pwf)
  {
    std::cerr << "passwd file error\n";
    std::exit(EIO);
  }

  uid_t in_file_uid;
  while (pwf >> in_file_uid)
  {
    char colon;
    if (!(pwf >> colon && colon == ':'))
    {
      std::cerr << "invalid passwd file, expected ':'\n";
      std::exit(EILSEQ);
    }

    if (in_file_uid == uid)
    {
      return true;
    }
    else
    {
      pwf.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    user_pos = pwf.tellg();
  }

  return false;
}

bool Passwd_db::check_pw()
{
  if (!find_user())
  {
    std::cerr << "You aren't in passwd file!\n";
    return false;
  }
  Password entered_pw = ask_pw("Password: ");

  Password pw_in_f;
  std::getline(pwf, pw_in_f);
  Password pwh = hash_pw(std::move(entered_pw), pw_in_f);

  return is_pw_equal(pw_in_f, pwh);
}

bool Passwd_db::check_pw(Password pw)
{
  if (!find_user())
  {
    std::cerr << "You aren't in passwd file!\n";
    return false;
  }

  Password pw_in_f;
  std::getline(pwf, pw_in_f);
  Password pwh = hash_pw(std::move(pw), pw_in_f);

  return is_pw_equal(pw_in_f, pwh);
}

bool Passwd_db::change_pw()
{
  if (find_user())
  {
    if (!check_pw())
    {
      std::cerr << "Invalid password\n";
      return false;
    }
  }

  Password entered_pw = ask_pw("New password: ");
  {
    Password entered_pw2 = ask_pw("Retype password: ");

    if (entered_pw != entered_pw2)
    {
      std::cerr << "passwords don't match!\n";
      return false;
    }
  }

  Password pwh = hash_pw(std::move(entered_pw));

  if (find_user())
  {
    pwf.clear();
    pwf.seekg(0);
    Password content;
    Password line;
    auto     uid_s = std::to_string(uid) + ':';
    while (std::getline(pwf, line))
    {
      if (line.substr(0, uid_s.size()) != std::string_view{uid_s})
      {
        content += line + '\n';
      }
    }
    pwf.close();
    pwf.open(pw_path, std::ios::in | std::ios::out | std::ios::trunc);
    pwf << content;
  }

  pwf.clear();
  pwf.seekp(0, std::ios::end);
  pwf << uid << ':' << pwh << "\n";

  return true;
}

} // namespace isi
