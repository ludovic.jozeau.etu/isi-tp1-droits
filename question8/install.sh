#!/usr/bin/env bash

cd "$(dirname "$0")"

rm -f rmg
cp ../build/rmg .
sudo chown admin:admin rmg
sudo chmod u+s rmg
