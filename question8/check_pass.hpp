#pragma once

#include "Password.hpp"

#include <unistd.h>

#include <fstream>

namespace isi
{
/**
 * @brief manipulate passwd db
 *
 * TODO: should lock passwd file
 *
 */
class Passwd_db
{
private:
  uid_t        uid;
  std::fstream pwf;

  std::fstream::pos_type user_pos{0};

public:
  Passwd_db(uid_t uid = getuid());

  bool find_user();
  bool check_pw();
  bool check_pw(Password pw);
  bool change_pw();
};

} // namespace isi