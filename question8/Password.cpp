#include "Password.hpp"

#include <crypt.h>

#include <cstring>
#include <iostream>

namespace isi
{

bool is_pw_equal(std::string_view lhs, std::string_view rhs)
{
  volatile bool equal{lhs.size() == rhs.size()};

  for (std::size_t i = 0; i < lhs.size() && i < rhs.size(); ++i)
  {
    equal = equal && lhs[i] == rhs[i];
  }
  // TODO: maybe add a random sleep between 0 and ?

  return equal;
}

Password hash_pw(Password&& pw)
{
  char* salt = crypt_gensalt("$y$", 5, nullptr, 0);
  if (!salt)
  {
    std::cerr << "cannot generate salt with yescrypt\n";
    std::exit(errno);
  }
  char* c_pwh = crypt(pw.c_str(), salt);
  std::fill_n(salt, std::strlen(salt), 0);
  Password pwh = c_pwh;
  std::fill_n(c_pwh, pwh.size(), 0);
  return pwh;
}

Password hash_pw(Password&& pw, Password const& salt)
{
  char*    c_pwh = crypt(pw.c_str(), salt.c_str());
  Password pwh = c_pwh;
  std::fill_n(c_pwh, pwh.size(), 0);
  return pwh;
}

Password ask_pw(std::string_view question)
{
  Password entered_pw;
  entered_pw.reserve(40);
  std::cout << question;
  {
    Password_mode pw_mode;
    std::getline(std::cin, entered_pw);
  }
  std::cout << "\n";

  return entered_pw;
}

} // namespace isi