#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  if (argc < 2)
  {
    printf("usage: %s file\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  printf("euid: %d, egid: %d, ruid: %d, rgid: %d\n",
         geteuid(),
         getegid(),
         getuid(),
         getgid());

  FILE* f = fopen(argv[1], "r");
  if (f == NULL)
  {
    perror("Cannot open file");
    exit(EXIT_FAILURE);
  }
  printf("Content of %s:\n", argv[1]);

  char buf[50] = {};
  while (fgets(buf, sizeof(buf), f) != NULL)
  {
    printf("%s", buf);
  }
  fclose(f);
  exit(EXIT_SUCCESS);

  return 0;
}
