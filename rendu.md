# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Jozeau, Ludovic, ludovic.jozeau.etu@univ-lille.fr

- Nom, Prénom, email: Delacroix, Louis, louis.delacroix.etu@univ-lille.fr 
 
## Question 1

Le processus ne pourra pas écrire dans le fichier titi.txt en effet, toto n'a pas les droits en écriture sur ce fichier, même si il fait partie du groupe ubuntu.
Le système regarde le triplet user en premier et si on est l'utilisateur du fichier il ne va pas considerer le triplet group. Puis il vérifie le groupe et enfin il vérifie le others. 

## Question 2

- Le caractère x pour un répertoire correspond au droit d'accès au répertoire. "Exécuter" un 
répertoire revient à y accéder.
- On ne peut pas accéder au dossier avec l'utilisateur toto parce que l'utilisateur qui a créé ce dossier est ubuntu et en retirant le droit d'execution pour le groupe ubuntu on empêche toto d'y accéder via son groupe ubuntu.
- On peut lister tous les fichiers et dossiers qui se trouvent dans mydir mais on ne peut pas connaitre les metadonnées (droits, utilisateur, groupe, data, etc.). Parce qu'on a le droit de lecture mais pas d'exécution donc on peut lire le contenu mais on ne peut rien faire à l'intérieur 

## Question 3

- avant le `set-user-id`:

```
euid: 1001, egid: 984, ruid: 1001, rgid: 984
Cannot open file: Permission denied
```

On remarque que le euid et le ruid sont identique, et que l'on arrive pas a lire le fichier.

après:

```
euid: 1000, egid: 984, ruid: 1001, rgid: 984
Content of ../mydir/data.txt:
test
```

On remarque bien que le euid a changé pour être celui de ubuntu au lieu de toto, et on arrive maintenant a lire le fichier.

## Question 4

avec le `set-user-id` sur suid.py:

```
euid: 1001, egid: 984, ruid: 1001, rgid: 984
```

on voit que euid est identique à ruid car ce n'est pas ce fichier qu'on execute mais python qui execute ce fichier.

on peut le verifier avec:

```bash
sudo chmod u+s /bin/python3.10
./suid.py
toto@7ccce343933b /home/ubuntu/exec % ./suid.py
euid: 0, egid: 984, ruid: 1001, rgid: 984
```

on voit que le euid change et est 0 (root)

## Question 5

`chfn` permet de modifier le nom complet et les informations associées à un utilisateur.

```bash
toto@arch /home/arch/exec % la /usr/bin/chfn  
Permissions Links Size User Group Date Modified Name
.rwsr-xr-x      1  27k root root   6 Jan 12:07  /usr/bin/chfn
```

Il est executable par tout le monde et le euid sera root on le voit au `s` qui est le set-uid

## Question 6

les hashs des mots de passe sont dans `/etc/shadow`:

```bash
toto@arch /home/arch/exec % la /etc/shadow
Permissions Links Size User Group Date Modified Name
.rw-------      1  759 root root  13 Jan 22:05  /etc/shadow
```

Pour qu'il ne soit pas accessible par tous les utilisateurs mais seulement par root afin d'augmenter la sécurité. C'est aussi pour ça que le mot de passe n'est pas stocké en clair. Pour que les utilisateurs ayant le mot passe root ne connaisse pas le mot de passe des autres utilisateurs.

## Question 7

Voici les droits finaux pour le dossier share:

```
Permissions Links Size User  Group    Date Modified Name
drwxr-xr-x      5    - admin admin 19 Jan 17:24  share
drwxrws--T      2    - admin groupe_a 19 Jan 17:55  dir_a
drwxrws--T      2    - admin groupe_b 19 Jan 17:55  dir_b
drwxr-s---      2    - admin groupe_c 19 Jan 17:55  dir_c
```

- Le script de création du dossier share: [./question7/create_share.sh](./question7/create_share.sh)
- Le script de test: [./question7/test_share.sh](./question7/test_share.sh)
- Les utilities de test: [./question7/test_share_utils.sh](./question7/test_share_utils.sh)

Pour la partie:
> peuvent modifier TOUS les fichiers contenus dans l’arborescence à partir de dir_a

Par défaut quand on crée un fichier le groupe ne peut que lire et pas ecrire, il y a plusieurs moyen mais ils ont tous des inconvénients:

- utiliser `umask` dans le fichier profile -> inconvenient s'applique en dehors du dossier share
- creer un script pour creer et éditer des fichiers -> il faut toujours utiliser le script non enforcable
- un script qui tous les jours/heures... fait un chmod pour autoriser l'écriture par le groupe
- utiliser ACL ?

voici le resultat des tests:

[![asciicast](https://asciinema.org/a/d2hhCZbcW13eY9YLBlLeOsyI6.svg)](https://asciinema.org/a/d2hhCZbcW13eY9YLBlLeOsyI6)

## Question 8

- Le programme rmg: [./question8/rmg.cpp](./question8/rmg.cpp)
- Les fonctions autour de passwd (`check_pw` et `change_pw`): [./question8/check_pass.cpp](./question8/check_pass.cpp) [./question8/check_pass.hpp](./question8/check_pass.hpp)

Ce programme supporte le mode noecho pour demander le mot de passe, ainsi que la mise à 0 de la mémoire où est stocké le mot de passe

Pour le compiler et "l'installer" avec les bonnes permissions dans le dossier question8:

```bash
mkdir build
cd build
cmake .. # -DSHARE_FOLDER=path/to/share
cd ../question8
./install.sh
./rmg path/to/share/dir_a/file_to_remove
```

Les tests sont avec la question9

## Question 9

- Le programme pwg: [./question8/rmg.cpp](./question9/pwg.cpp)

`crypt` est utilisé avec yescrypt

Pour le compiler et "l'installer" avec les bonnes permissions dans le dossier question8:

```bash
mkdir build
cd build
cmake .. # -DSHARE_FOLDER=path/to/share
cd ../question9
./install.sh
./pwg
```

Le script de test: [./question9/test_rmg_pwg.sh](./question9/test_rmg_pwg.sh)
Le résultat:

[![asciicast](https://asciinema.org/a/RrBvuoPoFfdTcwO6MNJZ9FNQp.svg)](https://asciinema.org/a/RrBvuoPoFfdTcwO6MNJZ9FNQp)

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 

### Compilation

/!\  Les programmes nécessite l'installation des librairies boost/asio et boost/process:

sur ubuntu:

```bash
sudo apt install libboost-all-dev
```

Pour compiler les programmes *groupe_client* et *groupe_server* il suffit de taper les commandes suivantes:

```sh
mkdir build
cd build
cmake .. # -DSERVER_ADDR=<addr>
cmake --build . -j
```


### Usages

```bash
cd build
sudo ./group_server

./group_client cmd_file
# ou
./group_client <(echo "lambda_a lambdaa\nlist dir_a\n")
```

### Fonctionnement 

Le serveur reçoit la connexion fork son processus et traite, dans le processus fils l'authentification, il répond au client si l'authentification est correcte ou non, si l'authentification est correcte il set le euid et egid du processus fils.     
Le client envoie les commandes, le serveur les reçoit les traite et renvoie leurs sorties dans le socket de connexion.  