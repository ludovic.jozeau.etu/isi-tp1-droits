#!/usr/bin/env python3
import os

print(f"euid: {os.geteuid()}, egid: {os.getegid()}, ruid: {os.getuid()}, rgid: {os.getgid()}")
