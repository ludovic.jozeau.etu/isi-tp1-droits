#!/bin/bash
echo "####################"
echo "######lambda_a######"
echo "####################"
echo "lambda_a a" > cmd
echo "list dir_b">> cmd
echo "read dir_b/caca">> cmd
echo "list dir_a">> cmd
echo "read dir_b/caca">> cmd
echo "close">> cmd

../build/group_client cmd 1>response&
wait #Wait the child process
i=0
while read line
do
  j=0
  while read l
  do
      if [[ $j == $i ]]
      then
	      break
      fi
      j=$((j+1))
  done < expected_a
  t="Expected: "$l
  if [[ $line == $l ]]
  then
	  echo $t"    OK"
  else
	  echo $t"    NG "$line
  fi
  i=$((i+1))
done < response
echo " "
echo "####################"
echo "######lambda_b######"
echo "####################"
echo "lambda_b b" > cmd
echo "list dir_b">> cmd
echo "read dir_b/caca">> cmd
echo "list dir_a">> cmd
echo "read dir_b/caca">> cmd
echo "close">> cmd

../build/group_client cmd 1>response&
wait #Wait the child process
i=0
while read line
do
  j=0
  while read l
  do
      if [[ $j == $i ]]
      then
	      break
      fi
      j=$((j+1))
  done < expected_b
  t="Expected: "$l
  if [[ $line == $l ]]
  then
	  echo $t"    OK"
  else
	  echo $t"    NG "$line
  fi
  i=$((i+1))
done < response
