#include <boost/asio/ip/tcp.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>

constexpr std::string_view addr_server = SERVER_ADDR;

namespace ip = boost::asio::ip;

std::string username;
std::string password;

/**
 * Set the username and the password
 * @param stream the stream of the command file
 */
void setAuthentification(std::fstream& stream)
{
  stream >> username;
  stream >> password;
  stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

/**
 * Login to the server
 * @param stream The connection to the server
 * @return false if the connection failed else true
 */
bool connection(std::iostream& stream)
{
  stream << "USER " << username << "\nPASS " << password << "\n";
  std::string status;
  std::getline(stream, status);
  if (status == "Accepted")
  {
    return true;
  }
  std::cout << "Authentification failed: " << status << "\n";
  return false;
}

void printResult(std::iostream& stream)
{
  std::string line;
  while (std::getline(stream, line) && !line.empty())
  {
    std::cout << line << "\n";
  }
}

/**
 * Send the commands to the server
 * @param stream the stream of the command file
 * @return 0 if the commands are sended else -2
 */
int sendCommand(std::fstream& fstream)
{
  std::string cmd;
  while (std::getline(fstream, cmd) && !cmd.empty())
  {
    ip::tcp::iostream stream(addr_server, "4000");
    if (!stream)
    {
      std::cout << "Error: " << stream.error().message() << "\n";
      return -2;
    }

    if (!connection(stream))
    {
      return -2;
    }
    stream << cmd << "\n";
    printResult(stream);
  }
  return 0;
}

/**
 * The main function of the client
 * @param path the path to the command file
 * return 0 in normal end, 1 if there are too few argument, -1 if the file doesn't exist
 * and -2 if the authentification failed
 */
int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cout << "The command need the path of the command file in argument\n";
    return 1;
  }
  std::fstream fs(argv[1]);

  if (!fs)
  {
    std::cout << "The file doesn't exist\n";
    return EXIT_FAILURE;
  }

  setAuthentification(fs);
  return sendCommand(fs);
}
