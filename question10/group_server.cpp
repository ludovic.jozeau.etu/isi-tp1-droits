#include "check_pass.hpp"
#include "user.hpp"

#include <boost/asio/ip/tcp.hpp>
#include <boost/process/io.hpp>
#include <boost/process/pipe.hpp>
#include <boost/process/system.hpp>

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <string_view>

namespace ip = boost::asio::ip;
namespace bp = boost::process;

boost::asio::io_context ioc;

ip::tcp::endpoint endpoint(ip::tcp::v4(), 4000);
ip::tcp::acceptor acceptor(ioc, endpoint);

const std::string share_folder{SHARE_FOLDER};

void launchCommand(std::iostream& stream)
{
  std::string cmd;
  std::string arg;
  stream >> cmd;
  if (cmd == "list")
  {
    cmd = "ls -al " + share_folder + "/";
  }
  else if (cmd == "read")
  {
    cmd = "cat " + share_folder + "/";
  }
  else if (cmd == "close")
  {
    stream << "End of connection\n";
    std::cout << "End of connection\n";
    exit(EXIT_SUCCESS);
  }
  else
  {
    stream << "unknown command " << cmd << "\n";
    std::cout << "unknown command " << cmd << "\n";
    exit(EXIT_FAILURE);
  }

  if (!(stream >> arg))
  {
    std::cerr << "socket error reading command args\n";
    std::exit(EXIT_FAILURE);
  }
  cmd += arg;
  std::cout << "launching command '" << cmd << "'\n";

  bp::ipstream is; // pipe
  bp::system(cmd, (bp::std_out & bp::std_err) > is);
  std::string line;

  while (std::getline(is, line) && !line.empty())
  {
    stream << line << "\n";
  }
}

void client(std::unique_ptr<std::iostream> stream_ptr)
{
  std::iostream& stream = *stream_ptr;
  std::string    name;
  if (!(stream >> name))
  {
    std::cerr << "socket error reading USER\n";
    std::exit(EXIT_FAILURE);
  }
  if (name != "USER")
  {
    std::cerr << "USER command expected\n";
    stream << "USER command expected\n";
    exit(EXIT_FAILURE);
  }
  struct passwd* pwd;
  try
  {
    if (!(stream >> name))
    {
      std::cerr << "socket error reading user\n";
      std::exit(EXIT_FAILURE);
    }

    pwd = getUID(name);

    std::string pass;
    if (!(stream >> pass))
    {
      std::cerr << "socket error reading PASS\n";
      std::exit(EXIT_FAILURE);
    }
    if (pass != "PASS")
    {
      std::cerr << "PASS command expected\n";
      stream << "PASS command expected\n";
      exit(EXIT_FAILURE);
    }
    if (!(stream >> pass))
    {
      std::cerr << "socket error reading pass\n";
      std::exit(EXIT_FAILURE);
    }

    isi::Passwd_db pw_db(pwd->pw_uid);
    if (!pw_db.find_user())
    {
      std::cerr << "Not in password database\n";
      stream << "Not in password database\n";
      exit(EXIT_FAILURE);
    }
    if (!pw_db.check_pw(std::move(pass)))
    {
      std::cerr << "Invalid login\n";
      stream << "Invalid login\n";
      exit(EXIT_FAILURE);
    }

    std::cout << "Accepted " << name << "\n";
    stream << "Accepted\n";
    stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  catch (std::runtime_error const& e)
  {
    stream << "Failed: " << e.what() << "\n";
    exit(EXIT_FAILURE);
  }

  std::cout << "setting euid: " << pwd->pw_uid << ", egid: " << pwd->pw_gid << "\n";

  if (setegid(pwd->pw_gid) != 0)
  {
    perror("setegid");
    stream << "Internal error\n";
    exit(EXIT_FAILURE);
  }
  if (seteuid(pwd->pw_uid) == 0)
  {
    launchCommand(stream);
    exit(EXIT_SUCCESS);
  }
  else
  {
    perror("seteuid");
    stream << "Internal error\n";
    exit(EXIT_FAILURE);
  }
}

int main()
{
  using namespace std::literals;
  while (true)
  {
    std::unique_ptr<ip::tcp::iostream> stream = std::make_unique<ip::tcp::iostream>();
    acceptor.accept(stream->socket());
    stream->expires_after(10s);
    std::cout << "Connection accepted\n";

    switch (fork())
    {
    case -1:
      throw std::runtime_error("An error occurred");
      break;
    case 0:
      client(std::move(stream));
      break;
    default:
      break;
    };
  }
  return 0;
}
