#include "user.hpp"
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <string>
#include <sys/types.h>

struct passwd* getUID(std::string const& name)
{
  struct passwd* pass = getpwnam(name.c_str());
  if (pass != NULL)
  {
    return pass;
  }
  else
  {
    throw std::runtime_error("User doesn't exist");
  }
}
