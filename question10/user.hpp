#pragma once

#include <pwd.h>
#include <string>
#include <sys/types.h>

struct passwd* getUID(std::string const& name);
