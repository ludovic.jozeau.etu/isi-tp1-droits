#!/usr/bin/env bash

cd "$(dirname $0)"
cd question7
./del_share.sh
./create_share.sh

cd ..
mkdir build
cd build
cmake .. -DSHARE_FOLDER=/home/share
cmake --build . -j

cd ../question8
./install.sh

cd ../question9
./install.sh

cd ..

sudo cp -a ./question8/rmg /usr/local/bin/
sudo cp -a ./question9/pwg /usr/local/bin/
